
const {
	http
} = uni.$u
// 获取菜单
export const fetchMenu = (params, config = {}) => http.post('/app/index/class', params, config)
// export const index = (params, config = {}) => http.get('/app/index/class', params, config)
export const getMenu = (data) => http.get('/api/index/class', data)
/**
 * 获取首页分类
 * @param {*} data 
 */
export const getClass = (data) => http.get('/api/index/class', data)
/**
 * 扩展分类
 * @param {*} data 
 */
export const getExtraClass = (data) => http.get('/api/index/extraClass')
/**
 *  滑动内容
 * @param {id} data 
 */
export const getScrollClass = (data) => http.get('/api/index/scrollClass/'+ data)
/**
 * 猜你喜欢
 * @param {*} data 
 */
export const getFavorite = (data) => http.get('/api/index/favorite', data)
/**
 * 分类展示
 * @param {*} data 
 */
export const getPageView = (data) => http.get('/api/index/pageView/' + data)
/**
 * 获取分类条目视频数据 
 * @param {*} data  pageView id
 */
export const getPageVodList = (data) => http.get('/api/index/view/' + data)
/**
 * 
 * @param {*} typeId 
 */
export const getBanner = (typeId) => http.get('/api/index/banner/' + typeId)

/**
 * 获取筛选分类数据
 */
export const filterVod = (params, config = {}) => http.post('/api/vod/filter', params, config)