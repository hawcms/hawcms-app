
const {
	http
} = uni.$u
/**
 * 获取视频详情
 * @param {*} id 
 */
export const getVodDetail = (id) => http.get('/api/vod/detail/' + id)

/**
 * 增加播放记录
 */
export const addRecord = (id) => http.post('/api/vod/record/'+ id)
/**
 * 搜索
 * @param {*} id 
 */
export const search = (id) => http.get('/api/vod/search/'+ id)
/**
 * 获取播放记录
 */
export const getRecord = () => http.get('/api/vod/record')

/**
 * 获取播放器
 */
export const getPlayer = (id) => http.get('/api/player/player/'+ id)

export const getVideoUrl = (url,config = {custom:{notIntercept:true}}) => http.get(url,config)