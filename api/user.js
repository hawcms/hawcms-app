const {
	http
} = uni.$u
/**
 * 登录
 */
export const login = (params, config = {}) => http.post('/api/user/login', params, config)
/**
 * 获取用户信息
 * @param {*} params 
 * @param {*} config 
 */
export const info = (params, config = {}) => http.get('/api/user/info', params)