import * as page from '@/util/core/pageService'
import {
	ACCESS_TOKEN
} from '@/common/constant'
export default {
	data() {
		return {

		}
	},
	methods: {
		getToken() {
			return uni.getStorageSync(ACCESS_TOKEN)
		},
		setToken(token) {
			uni.setStorage({
				key: ACCESS_TOKEN,
				data: token
			});
		},
		/** 跳转页面 必须在pages.json 注册 并且非tar页面
		 * @param {Object} url
		 * @param {Object} param
		 */
		navigateTo(url, param) {
			//在起始页面跳转到test.vue页面并传递参数
			//  /pages/components/vDetail/vDetail
			let p = null
			if (param instanceof Map) {
				param.forEach((k, v) => {
					// k 是参数 v 是值
					if (p == null) {
						p = '?'
					} else {
						p += '&'
					}
					const kv = k + '=' + v
					p += kv
				})
			}
			if (p != null) {
				url = url + p ? p : ''
			}
			console.log('页面参数,,,', url);
			page.navigateTo(url)
		}
	}
}
