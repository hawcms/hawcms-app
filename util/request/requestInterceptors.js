import { ACCESS_TOKEN, REWRITE_URL, BASE_PREFIX, REWRITE_PREFIX } from '@/common/constant'

/**
 * 请求拦截
 * @param {Object} http
 */
module.exports = (vm) => {
    uni.$u.http.interceptors.request.use((config) => { // 可使用async await 做异步操作
        // 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
		// 这里对url做处理
		// config.url = 
		if(config.custom.notIntercept){
			config.baseURL = ''
			return config
		}
		// ##ifdef APP-PLUS
		config.baseURL = REWRITE_URL
		config.url = config.url.replace(BASE_PREFIX,REWRITE_PREFIX)
		// ##endif
        config.data = config.data || {}
		// 让每个请求带上token
		const token = uni.$haw.token.getToken()
		if(token){
			config.header[ACCESS_TOKEN] = token
		}
        return config
    }, (config) => // 可使用async await 做异步操作
        Promise.reject(config))
}
