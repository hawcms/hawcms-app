// 引入配置
import { BASE_URL } from '@/common/constant'
const custom={
	catch: true
}
// 初始化请求配置
uni.$u.http.setConfig((defaultConfig) => {
    /* defaultConfig 为默认全局配置 */
    defaultConfig.baseURL = BASE_URL /* 根域名 */
	defaultConfig.custom = custom
    return defaultConfig
})

module.exports = (vm) => {
    require('./requestInterceptors')(vm)
    require('./responseInterceptors')(vm)
}
