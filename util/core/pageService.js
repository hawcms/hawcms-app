
/** 跳转页面 必须在pages.json 注册 并且非tar页面
 * @param {Object} url
 * @param {Object} param
 */
export function navigateTo(url, param = new Map()) {
	//在起始页面跳转到test.vue页面并传递参数
	//  /pages/components/vDetail/vDetail
	let p = null
	if (param instanceof Map) {
		param.forEach((k, v) => {
			// k 是参数 v 是值
			if (p == null) {
				p = '?'
			} else {
				p += '&'
			}
			const kv = k + '=' + v
			p += kv
		})
	}
	if (p != null) {
		url = url + p ? p : ''
	}
	uni.navigateTo({
		url: url
	});
}
