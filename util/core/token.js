import {
	ACCESS_TOKEN
} from '@/common/constant'
export function getToken() {
	return uni.getStorageSync(ACCESS_TOKEN)
}
export function setToken(token) {
	uni.setStorage({
		key: ACCESS_TOKEN,
		data: token
	});
	if(!token){
		// 跳转登录页
		uni.navigateTo({
			url: '/pages/user/login'
		});
	}
}