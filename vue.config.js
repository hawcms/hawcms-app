module.exports = {
	devServer: {
		port: 9527,
		disableHostCheck: true,
		proxy: {
			'/api': {
				target: 'http://192.168.0.101:8080',
				changeOrigin: true,
				pathRewrite: {
					'^/api': 'app'
				}
			}
		}
	}
}
